function calcular() {
    let valor1 = document.getElementById('primeiro')
    let valor2 = document.getElementById('segundo')
    let operador = document.getElementById('operador')
    let total = "";
    let formulario = document.querySelector('.formulario')

    valor1 = parseFloat(valor1.value)
    valor2 = parseFloat(valor2.value)
    operador = operador.value

    let sucesso = document.createElement('div')
    sucesso.className = 'sucesso alert alert-primary text-center'
    let erro = document.createElement('div')
    erro.className = 'erro alert alert-danger text-center'
    erro.innerHTML = "<strong>Opsss...</strong> você precisa informar os valores e o operador!"

    console.log(valor1)
    console.log(valor2)
    console.log(operador)

    if(document.querySelector('.erro')){
        mostra_erro = document.querySelector('.erro')
        mostra_erro.parentNode.removeChild(mostra_erro)
    }

    if (operador == "soma" && (valor1 != "" && !isNaN(valor1)) && (valor2 != "" && !isNaN(valor1))) {
        total = valor1 + valor2
        sucesso.innerHTML = "<strong>Operador selecionado:</strong> Soma<br> <strong>Total:</strong> " + total
        formulario.appendChild(sucesso)
    }

    if (operador.value == "sub" && valor1.value != "" && valor2 != "") {
        total = valor1 - valor2
        sucesso.innerHTML = "<strong>Operador selecionado:</strong> Subtração<br> <strong>Total:</strong> " + total
        formulario.appendChild(sucesso)
    }

    if (operador.value == "mult" && valor1 != "" && valor2 != "") {
        total = valor1 * valor2
        sucesso.innerHTML = "<strong>Operador selecionado:</strong> Multiplicação<br> <strong>Total:</strong> " + total
        formulario.appendChild(sucesso)
    }

    if (operador.value == "div" && valor1.value != "" && valor2.value != "") {
        total = valor1 / valor2
        sucesso.innerHTML = "<strong>Operador selecionado:</strong> Divisão<br> <strong>Total:</strong> " + total
        formulario.appendChild(sucesso)
    }

    if (isNaN(operador) || isNaN(valor1) || isNaN(valor2)) {
        formulario.appendChild(erro)
    }
}