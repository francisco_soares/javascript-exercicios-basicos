//[DESCRIÇÃO DA ATIVIDADE]
/**
Peça para o usuário digitar uma data. Exiba separadamente o dia, o mês e o ano. 
(Obs.: não necessita de laço de repetição)
 */

function recebeData() {
    /** Pego o valor via [ID] que vem do formulário */
    var data = document.getElementById('data').value

    /**
     * Falo as seguintes condições:
     * 1º - Verifico se da nada digitada foi a barra(/), caso seja, entro na condição e transformo os dados recebido em um [Array]
     * 
     * 2º - Verifico se da nada digitada foi a traço(-), caso seja, entro na condição e transformo os dados recebido em um [Array]
     * 
     * 3º - Caso a condição 1 e 2 não sejam atendidas, eu caio no [ELSE] e mostro o erro.
     */
    if (data.indexOf('/') > 0) {
        data = data.split('/')
    } else if (data.indexOf('-') > 0) {
        data = data.split('-')
    } else {
        document.getElementById('error').innerHTML = "Opss... Você deve informar uma data como mostra o exemplo: <strong>10/02/1992 ou 10-02-1992</strong>"
        document.getElementById('error').style.padding = "10px 20px"
        document.getElementById('error').style.border = "2px solid #db3939"
        return false
    }

    /**
     * Verifico dentro do [Array] se os indices passam de 3, caso seja atendida, imprimo o erro e faço a aplicação parar.
     */
    if (data.length > 3) {
        document.getElementById('error').innerHTML = "Opss... Você deve informar apenas <strong>UMA</strong> data como mostra o exemplo: <strong>10/02/1992 ou 10-02-1992</strong>"
        document.getElementById('error').style.padding = "10px 20px"
        document.getElementById('error').style.border = "2px solid #db3939"
        return false
    }

    /**
     * Abaixo crio as váriares e mostro os valores separadamente sem um loop.
     */
    var dataDia = document.createElement('span')
    dataDia.innerHTML = "<strong>Dia:</strong> " + data[0] + " | "

    var dataMes = document.createElement('span')
    dataMes.innerHTML = "<strong>Mês:</strong> " + data[1] + " | "

    var dataAno = document.createElement('span')
    dataAno.innerHTML = "<strong>Ano:</strong> " + data[2]

    document.getElementById('success').appendChild(dataDia)
    document.getElementById('success').appendChild(dataMes)
    document.getElementById('success').appendChild(dataAno)
    document.getElementById('success').style.padding = "10px 20px"
    document.getElementById('success').style.border = "2px solid #3fcc4d"

}