//[DESCRIÇÃO DA ATIVIDADE]
/*
Peça ao usuário para digitar 5 números em uma caixa de texto. Verifique qual é o maior número e exiba-o.
*/

function retornaMaiorNumero() {
    /** Pego os valores digitados no formulário e seto na variável [members] */
    var numbers = document.getElementById('numbers').value

    /** Retiro a virgula(,) e transformo todo os valores em um array dentro da variável [members] */
    numbers = numbers.split(',')    

    /** Pego o maior número do [Array numbers] */
    var bigNumber = Math.max.apply(null, numbers)

    /** 
     * Faço uma validação verificando se a variável [bigNumber] é [isNaN] ou está [vazia], se caso atenda algumas das opções, ele entra dentro do [IF] e imprime o erro e restura toda a função como [FALSE]. Assim o código morre aqui. */
    if (isNaN(bigNumber) || numbers == '') {
        document.getElementById('error').innerHTML = "Opss... Por favor, insira apenas números como mostra o exemplo: <strong>5, 9, 10, 4, 6</strong>"
        document.getElementById('error').style.padding = "10px 20px"
        document.getElementById('error').style.border = "2px solid #db3939"
        return false
    }

    /**
     * Entro no laço [FOR] apenas para percorrer a quantidade de elemntos da váriavel [numbers] com a propriedade [LENGTH].
     * 
     * Dentro do [FOR] eu faço uma condição com o [IF].
     * Se a quantidade de elementos na variável [numbers] for MENOR que 5 OU MAIOR que 5, imprimi o erro. Assim o código morre aqui. 
     */
    for (var i = 0; i < numbers.length; i++) {

        if (numbers.length < 5 || numbers.length > 5) {
            document.getElementById('error').innerHTML = "Opss... Você precisa insirir apenas <strong>5 números</strong> como mostra o exemplo: <strong>5, 9, 10, 4, 6</strong>"
            document.getElementById('error').style.padding = "10px 20px"
            document.getElementById('error').style.border = "2px solid #db3939"
            return false
        }
    }

    /**
     * Crio um elemento [H3] na [DIV ID success] com um texto.     * 
     */
    document.getElementById('success').innerHTML = "<h3 class='fs-6'>Você digitou os seguintes números:</h3>"

    /**
     * Entro em um laço [FOR] para percorrer os valores do [Array numbers] e imprimir os valores.
     */
    for (var j = 0; j < numbers.length; j++) {
        var div = document.createElement('span')
        div.innerHTML = '| <strong>' + numbers[j] + '</strong> '


        document.getElementById('success').style.padding = '10px 20px'
        document.getElementById('success').style.border = "2px solid #3fcc4d"
        document.getElementById('success').appendChild(div)
    }

    /**
     * Abaixo crio uma [DIV] e imprimo o maior valor dentro do [Array numbers]
     */
    var divResult = document.createElement('p')
    divResult.innerHTML = "<br>O número <strong>Maior</strong> digitado é: <strong>" + bigNumber + "</strong>"
    
    document.getElementById('success').appendChild(divResult)

}