//[DESCRIÇÃO DA ATIVIDADE]
/*
Faça um programa que entre com cinco números e imprima o quadrado de cada número.
*/
function calculoQuadrado() {
    /** Pego os valores digitados no formulário e seto na variável [members] */
    var numbers = document.getElementById('numbers').value

    /** Retiro a virgula(,) e transformo todo os valores em um array dentro da variável [members] */
    numbers = numbers.split(',')

    /** Entro no loop comparando a quantidade e elementos que tem no array [members] junto com o método [.length] */
    for (var i = 0; i < numbers.length; i++) {
        /** Faço a operação para saber o quadrado de cada número. FORMULA: valor1 * valor1 */
        var numberResult = (numbers[i].trim() * numbers[i].trim())
        /** Converto os valores em Float com o [parseFloat()] */
        numberResult = parseFloat(numberResult)

        /**
         * Faço a decisão no if.
         * Caso os números digitados forem: LETRAS ou NÚMEROS menor que 5 ou NÚMEROS maior que 5
         * retorna o erro 
         */
        if (isNaN(numberResult) || numbers.length < 5 || numbers.length > 5) {
            /** Pego a [DIV ID - error] e mostro a mensagem e também coloco uma estilização [CSS] */
            document.getElementById('error').innerHTML = "Opss... Por favor, insira apenas números como mostra o exemplo: <strong>5, 9, 10, 4, 6</strong>"
            document.getElementById('error').style.padding = "10px 20px"
            document.getElementById('error').style.border = "2px solid #db3939"
            break
        } else {
            /** Crio uma div com a [CLASS - result] e imprimo os valores com o [innerHTML]*/
            var div = document.createElement("div");
            div.className = 'result'
            div.innerHTML = "O Quadrado do número: <strong> " + numbers[i].trim() + "</strong> é: <strong>" + numberResult + "</strong>";

            /** Abaixo implemento a [CLASS - result] dentro do [ID - success] */
            document.getElementById('success').style.padding = "10px 20px"
            document.getElementById('success').style.border = "2px solid #3fcc4d"
            document.getElementById('success').appendChild(div)
        }
    }
}