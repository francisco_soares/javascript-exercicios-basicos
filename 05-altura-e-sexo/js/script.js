//[DESCRIÇÃO DA ATIVIDADE]
/**
 Crie um programa que entre com os dados de altura e sexo de 5 pessoas. 
 Imprima na tela quantas pessoas são do sexo masculino e quantas pessoas são do sexo feminino. 
 Mostre também qual é a maior altura e se essa altura é de um homem ou uma mulher.
 */

function verificaAlturaESexo() {
    var altura = document.getElementById('altura').value
    var sexo = document.getElementById('sexo').value

    altura = altura.split(',')
    sexo = sexo.split(',')

    var masculino = 0
    var feminino = 0

    var maisAlto = 0
    var oSexo = null

    for (var i = 0; i < altura.length; i++) {

        var alturaFloat = parseFloat(altura)

        if (isNaN(alturaFloat) || altura.length < 5 || altura.length > 5) {
            document.getElementById('error').innerHTML = "Opss... Você deve apenas informar <strong>Números</strong> como mostra o exemplo: <strong>175,180,170,165,182</strong>"
            document.getElementById('error').style.padding = "10px 20px"
            document.getElementById('error').style.border = "2px solid #db3939"
            return false
        }
    }

    for (var j = 0; j < sexo.length; j++) {

        if (sexo.length < 5 || sexo.length > 5 || sexo[j] !== 'm' && sexo[j] !== 'f') {

            document.getElementById('error').innerHTML = "Opss... Você deve apenas informar <strong>LETRAS</strong> como mostra o exemplo: <strong>f,m,f,f,m</strong>"
            document.getElementById('error').style.padding = "10px 20px"
            document.getElementById('error').style.border = "2px solid #db3939"
            return false

        } else if (sexo[j] === "m") {
            masculino += 1
        } else {
            feminino += 1
        }

        if (maisAlto < parseFloat(altura[j])) {
            maisAlto = parseFloat(altura[j])
            oSexo = sexo[j]

            if (oSexo === "m") {
                oSexo = "Masculino"
            } else {
                oSexo = "Feminino"
            }
        }
    }

    // maisAlto
    // oSexo

    var tituloResultado = document.createElement('div')
    tituloResultado.innerHTML = "<h3 class='text-center text-white fs-4 fw-bold'>Resultado</h3>"
    document.getElementById('success').appendChild(tituloResultado)


    var qtdMasculino = document.createElement('div')
    qtdMasculino.innerHTML = '<span class="fs-6 d-block"><strong>Quantidade Masculino:</strong> ' + masculino + '</span>'
    document.getElementById('success').appendChild(qtdMasculino)

    var qtdFeminino = document.createElement('div')
    qtdFeminino.innerHTML = '<span class="fs-6 d-block"><strong>Quantidade Feminino:</strong> ' + feminino + '</span>'
    document.getElementById('success').appendChild(qtdFeminino)

    var mostraMaisAlto = document.createElement('div')
    mostraMaisAlto.innerHTML = '<span class="fs-6"><strong>Altura do mais alto: </strong> ' + maisAlto + 'cm</span>'
    document.getElementById('success').appendChild(mostraMaisAlto)

    var mostraSexo = document.createElement('div')
    mostraSexo.innerHTML = '<span  class="fs-6"><strong>Sexo do(a) mais Alto(a): </strong> ' + oSexo + '</span>'
    document.getElementById('success').appendChild(mostraSexo)

    document.getElementById('success').style.padding = "10px 20px"
    document.getElementById('success').style.border = "2px solid #3fcc4d"
}