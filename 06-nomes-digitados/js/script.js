//[DESCRIÇÃO DA ATIVIDADE]
/**
Peça ao usuário para digitar vários nomes. 
Exiba na tela todos os nomes digitados, porém de maneira invertida (do último para o primeiro).
 */

function variosNomes() {
    /** Pego os valores digitados no formulário pelo campo [ID] */
    nome = document.getElementById('nome').value
    /** Convertos os valores em um [Array] */
    nome = nome.split(',')
    /** Inverto as posições do [Array]  */
    nome = nome.reverse()
    /** Crio essa variável para fazer uma comparação no loop */
    var validacao = 0

    /** 
     * Uso o [.forEach(function (valor, indice, array){})] para percorrer o [Array] e pegar os valores. 
     */
    nome.forEach(function (valor, indice, array) {

        /** Faço a condição [IF] verificando se a variável [valor] está vázia.
         * Caso seja atendida, mostra o erro no [ID error].
         * 
         * Caso não seja atendida ou seja, a variável [valor] tenha algo, a condição entrará no [ELSE]
         */
        if (valor == '') {
            document.getElementById('error').innerHTML = "Opss... Você deve apenas informar <strong>NOMES</strong> como mostra o exemplo: <strong>Francisco, Isa, Saitama</strong>"
            document.getElementById('error').style.padding = "10px 20px"
            document.getElementById('error').style.border = "2px solid #db3939"
            return false
        } else {

            /** 
             * Lembra da variável [validacao] definida no começo do script, então, aqui está ela na condição.
             * Essa condição faz o seguinte: caso o valor da variável [validacao] seja igual à [0], entra na condição e faças o processo.
             */
            if (validacao === 0) {
                /** 
                 * Caindo na condição, aqui crio um elemento [DIV] com o comando [document.createElement('<elemento-para-criar>')] e
                 *  atribuo a variável [tituloResultado] 
                 **/
                var tituloResultado = document.createElement('div')

                /** Aqui uso o [.innerHTML = "<o-que-vou-inserir-no-elemento>"] para colocar elementos como [H3] e [class] e formata-lá */
                tituloResultado.innerHTML = "<h3 class='text-white fs-6 text-uppercase fw-bold'>Nomes digitados invertidos</h3>"
                /** Abaixo eu pego a váriavel [tituloResultado] com tudo que foi criado e mostro na [DIV ID success] */
                document.getElementById('success').appendChild(tituloResultado)

                /* Aqui atruibuo o valor de + 1 a variável [validacao], dessa forma, elá não entra mais nessa condição. */
                validacao++
            }

            /** Aqui crio um elemento [span] */
            var resultadoNomes = document.createElement('span')

            /** Faço uma condição verificando se a posição do [ARRAY nome] é a última posição e comparo com a variável [indice] do [forEach()]
             * Caso seja, retiro a última virgula que iria aparecer.
             */
            if (nome.length - 1 == indice) {
                resultadoNomes.innerHTML = valor
            } else {
                resultadoNomes.innerHTML = valor + ', '
            }

            /** 
             * Aqui apenas jogo a variável [resultadoNomes] para dentro da [DIV ID success] e a mostro.
             * Abaixo dela apenas formato a [CSS]
             */
            document.getElementById('success').appendChild(resultadoNomes)
            document.getElementById('success').style.padding = "10px 20px"
            document.getElementById('success').style.border = "2px solid #3fcc4d"
        }
    });
}