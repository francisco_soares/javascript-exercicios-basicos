function pegaCursos() {
    let outrosCursos = $("#outro")
    let cursos = []

    $("input[name='cursos[]']:checked").each(function () {
        cursos.push($(this).val())
    })

    if (($.inArray("outros", cursos) !== -1) && (outrosCursos.val() == "" || outrosCursos.val() == undefined)) {
        alert('Erro: Você precisa informar um cursos')
        return false
    }else{
        alert("Dados enviados com sucesso")
        for(let i = 0; i < cursos.length; i++){
            console.log(cursos[i])
        }
    }
}