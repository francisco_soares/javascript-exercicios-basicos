//[DESCRIÇÃO DA ATIVIDADE]
/**
 * Peça ao usuário para digitar várias idades. Exiba quantas pessoas são maior de idade (18 anos) e quantas são menores.
 */

function verificaMaiorIdade() {

    /** Pego o valor da variavel pelo [ID] */
    var age = document.getElementById('age').value
    /** Transformo os dados recebido em um [Array] */
    age = age.split(',')

    var ageOld = 0

    /** 
     * 1º - Entro no laço for onde pego a quantidade de elementos no [Array age] usando o [.length](Dúvida: use o console.log(age.length)).
     * 
     * 2º - Tranformo os valores do [Array] em um [INT] e depois entro na condição [IF] onde eu vejo se o resultado retorna [isNaN].
     * Caso retorno [isNaN], entra na condição e retorna a mensagem de erro direto HTML.
     * 
     * 3º - No [ELSE IF], apenas verifico se os dados digitados é MAIOR que 18. Caso seja, incrementa +1 na variavel [ageOld]
     */
    for (var i = 0; i < age.length; i++) {

        var ageInt = parseInt(age[i])

        if (isNaN(ageInt)) {
            document.getElementById('error').innerHTML = "Opss... Você deve apenas informar <strong>Números</strong> como mostra o exemplo: <strong>5, 9, 10, 4, 6</strong>"
            document.getElementById('error').style.padding = "10px 20px"
            document.getElementById('error').style.border = "2px solid #db3939"
            return false

        } else if (age[i] >= 18) {
            ageOld += 1
        }
    }

    /** Crio uma [DIV] e implemento uma tag [HTML H3] com o texto formatado com [Bootstrap] */
    var titleResult = document.createElement('div')
    titleResult.innerHTML = "<h3 class='text-center text-white fs-4 fw-bold'>Resultado:</h3>"

    /** Crio elemento [P] e implemento o texto */
    var data = document.createElement('p')
    data.innerHTML = "<p><strong>Maiores de idades:</strong> " + ageOld + "</p>"

    /** Aqui informo em qual local [DIV ID SUCCESS] deve implementar as váriaveis criadas acima. */
    document.getElementById('success').appendChild(titleResult)
    document.getElementById('success').appendChild(data)

    /** Inserido formatações [CSS] */
    document.getElementById('success').style.padding = "10px 20px"
    document.getElementById('success').style.border = "2px solid #3fcc4d"

    /** Crio uma [DIV] e implemento uma tag [HTML H3] com o texto formatado com [Bootstrap] */
    var ageText = document.createElement('h3')
    ageText.innerHTML = "<h3 class='text-center text-white fs-6 fw-bold'>Idades que foram digitadas:</h3>"

    /** Aqui informo em qual local [DIV ID SUCCESS] deve implementar a váriavel criada acima. */
    document.getElementById('success').appendChild(ageText)

    /**
     * Dentro do laço [FOR] apenas retorno os valores digitados 
     */
    for (var j = 0; j < age.length; j++) {
        var ageDes = document.createElement('span')
        ageDes.innerHTML = age[j] + ', '
        console.log(ageDes)
        document.getElementById('success').appendChild(ageDes)
    }
}