function validaFormulario(event) {

    // Pego os elementos pelo ID
    let formulario = document.getElementById("formulario")
    let nome = document.getElementById('nome')
    let email = document.getElementById('email')
    let idade = document.getElementById('idade')
    // Como é um campo do tipo [checkbox] eu tive que usar o [getElementsByName]
    let cursos = document.getElementsByName('cursos')
    let cursos_alerta = document.getElementById("cursos")
    let alerta_unico = document.querySelector('.alerta_unico')

    // console.log(cursos[0].value)
    // listaCursos = []

    // Abaixo crio uma variavel para usar como validação no if se caso todos os campos não estejam preenchidos.
    let verificar_checkbox = false
    // Crio esse for para usar para fazer uma validação juntamente com a variavel [verificar_checkbox] definida acima.
    for (let i = 0; i < cursos.length; i++) {
        // listaCursos.push(cursos[i].checked)

        // Aqui faço a validação, caso alguns dos elementos do [checkbox] seja clicado, a variavel [verificar_checkbox] recebe o valor de [TRUE]
        // assim dando um [break] para sair da consição [IF] e do laço [FOR]
        if (cursos[i].checked === true) {
            verificar_checkbox = true
            break;
        }
    }

    // console.log(listaCursos)


    // Para impressão de erro
    let alerta = document.querySelector('.alerta')

    // ******************** [VALIDAÇÃO] com innerHTML ******************** 
    // let alerta_erro = "<small class='text-danger'>Opss... esse campo é obrigatório!</small>"

    // if (nome.value == "" || nome.value == "undefined") {
    //     nome.style.border = '1px solid #d12121'
    //     alerta.innerHTML = alerta_erro
    //     event.preventDefault()
    // }

    // ******************** [VALIDAÇÃO] com Sobreposição de classe ******************** 

    // Crio o elemento [small]
    let alerta_erro = document.createElement('small')
    // Dentro do elemento [small] eu defino a classe [text-danger] com a propriedade [className]
    alerta_erro.className = "text-danger"
    // Coloco uma mensagem com o [innerHTML]
    alerta_erro.innerHTML = "Opss... todos os campos são obrigatório!"


    // Crio novamente outro elemento [small]
    let alerta_individual = document.createElement('small')
    // Dentro do elemento [small] eu defino a classe [text-danger] com a propriedade [className]
    alerta_individual.className = "text-danger"
    // Coloco uma mensagem com o [innerHTML]
    alerta_individual.innerHTML = "Opss... esse campo é obrigatório!"

    // Verifico se o elemento [small] foi criado, então entra dentro da condição e remove o próprio elemento [small]
    // Assim previno nos [else if] do [nome, email, idade e curso] que o alerta da variavel [alerta_individual] não mostre novamente depois da primeira vez.
    console.log(formulario.querySelector('small'))
    if (formulario.querySelector('small')) {
        let alertaVisivel = document.querySelector('small')
        alertaVisivel.parentNode.removeChild(alertaVisivel)
    }

    // Crio a condição para verificar se todos os campos estão vázio. Caso atenda todas as condições, entra no laço.
    // [LEMBRETE] - Lembra da variavel [verificar_checkbox] que defini acima? Ela está aqui dentro dessa condição. Se caso nenhum campo do [checkbox]
    // Não tenha sido clicado, a váriavel [verificar_checkbox] ficará com o valor de [FALSE] onde aqui irei vazer essa verificação.
    if (nome.value == "" && email.value == "" && idade.value == "" && verificar_checkbox === false) {

        // O [event.preventDefault()] para a ação do botão [submit] do formulário
        // [LEMBRETE] - o [event] é definido como um parametro dentro da função. Onde também deve ser definido no [onclick] que está no formulário HTML.
        event.preventDefault()

        // Como o elemento [small] ainda não foi definido, eu valido ele alterando para [true] com o [!]
        // Assim fazendo entrar dentro da condição. Da próxima vez que for cliclado, ele será [false] e assim não entra mais na condição.
        if (!formulario.querySelector('small')) {
            alerta.appendChild(alerta_erro)
            event.preventDefault()
        }
    } else if (nome.value == "") {

        // [ATENÇÃO] - O script abaixo não se faz mais necessário, porém, fica como dica de uso.
        // Como o elemento [small] ainda não foi definido, eu valido ele alterando para [true] com o [!]
        // Assim fazendo entrar dentro da condição. Da próxima vez que for cliclado, ele será [false] e assim não entra mais na condição.
        // if (!formulario.querySelector("small")) {
        //     nome.parentNode.insertBefore(alerta_individual, nome.nextSibling)
        //     event.preventDefault()
        // }        

        // Inserido a mensagem de alerta após o campo input do nome.
        nome.parentNode.insertBefore(alerta_individual, nome.nextSibling)
        event.preventDefault()


    } else if (email.value == "") {

        // [ATENÇÃO] - O script abaixo não se faz mais necessário, porém, fica como dica de uso.
        // Como o elemento [small] ainda não foi definido, eu valido ele alterando para [true] com o [!]
        // Assim fazendo entrar dentro da condição. Da próxima vez que for cliclado, ele será [false] e assim não entra mais na condição.
        // if (!formulario.querySelector("small")) {
        //     email.parentNode.insertBefore(alerta_individual, email.nextSibling)
        //     event.preventDefault()
        // }

        // Inserido a mensagem de alerta após o campo input do email.
        email.parentNode.insertBefore(alerta_individual, email.nextSibling)
        event.preventDefault()

    } else if (idade.value == "") {

        // [ATENÇÃO] - O script abaixo não se faz mais necessário, porém, fica como dica de uso.
        // Como o elemento [small] ainda não foi definido, eu valido ele alterando para [true] com o [!]
        // Assim fazendo entrar dentro da condição. Da próxima vez que for cliclado, ele será [false] e assim não entra mais na condição.
        // if (!formulario.querySelector("small")) {
        //     idade.parentNode.insertBefore(alerta_individual, idade.nextSibling)
        //     event.preventDefault()
        // }

        // Inserido a mensagem de alerta após o campo input do idade.
        if (!formulario.querySelector("small")) {
            idade.parentNode.insertBefore(alerta_individual, idade.nextSibling)
            event.preventDefault()
        }
    } else if (verificar_checkbox == false) {
        // Inserido a mensagem de alerta após o campo checkbox dos cursos.
        cursos_alerta.parentNode.insertBefore(alerta_individual, cursos_alerta.nextSibling)
        event.preventDefault()
    }

    // Verifico se o campo NOME não foi preenchido


    // [APENAS PARA LEMBRETE]
    // Esse [else] está desabilitado apenas para lembrar do código abaixo.
    // if (formulario.querySelector('small')) {
    //     let alertaVisivel = document.querySelector('small')
    //     alertaVisivel.parentNode.removeChild(alertaVisivel)            
    // }
}