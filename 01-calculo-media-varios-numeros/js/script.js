//[DESCRIÇÃO DA ATIVIDADE]
/*
Calcule a média de diversas notas digitadas pelo usuário.
*/

function calculoMedias() {
    /** Pego o valor da váriavel */
    var nota = document.getElementById('nota').value
    /** Crio um array com o [split] e insiro na variave nota  */
    nota = nota.split(',')

    var todasNotas = 0

    /** Dentro do FOR, com paro a quandidade de elementos dentro do array com o [.length] e faço rodar o loop*/
    for (var i = 0; i < nota.length; i++) {
        /** Seto somando o valor do array a variável todasNotas */
        todasNotas += parseFloat(nota[i].trim())

        if (isNaN(todasNotas)) {

            /** Seto o erro dentro da DIV com o ID (error) e implementos mais algumas formatações CSS */
            document.getElementById('error').innerHTML = "Opss... Por favor, insira apenas números como mostra o exemplo: <strong>5, 9, 10, 4, 6</strong>"
            document.getElementById('error').style.padding = "10px 20px"
            document.getElementById('error').style.border = "2px solid #db3939"
        }
    }

    /** 
     * Na condição, vejo se foi informado um caracter que resulte no [isNaN], exemplo: 5,ola,6
     * O OLA setado vai gerar o [isNaN]. Então, a aplicação vai funcionar apenas se preencher com NÚMEROS
     */
    if (isNaN(todasNotas)) {

        /** Seto o erro dentro da DIV com o ID (error) e implementos mais algumas formatações CSS */
        document.getElementById('error').innerHTML = "Opss... Por favor, insira apenas números como mostra o exemplo: <strong>5, 9, 10, 4, 6</strong>"
        document.getElementById('error').style.padding = "10px 20px"
        document.getElementById('error').style.border = "2px solid #db3939"
    } else {
        /** Faço o calculo com os valores digitados dividido pela quandidade de elementos dentro do array e implementos mais algumas formatações CSS */
        notaTotal = todasNotas / nota.length
        document.getElementById('success').innerHTML = "Sua média é de: <strong>" + notaTotal + "</strong>"
        document.getElementById('success').style.padding = "10px 20px"
        document.getElementById('success').style.border = "2px solid #3fcc4d"
    }
}